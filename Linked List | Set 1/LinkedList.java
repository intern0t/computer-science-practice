/**
 * Prashant Shrestha (https://prashant.me/)
 * Computer Science Practice
 */

public class LinkedList{
    Node head;
    static class Node{
        String data;
        Node next;

        Node(String d){
            data = d;
            next = null;
        }
    }

    /** Linked list traversal.  */
    public void printList(){
        Node currentNode = head;
        while(currentNode != null){
            System.out.print(currentNode.data + " ");
            currentNode = currentNode.next;
        }
    }

    /** Add new Node to our linked list. */
    public void addNewNode(Node _parentNode, Node _newNode){
        _parentNode.next = _newNode;
    }

    /** Add new Node to the front of our linked list. */
    public void pushNewNode(Node _newNode){
        _newNode.next = head;
        head = _newNode;
    }

    /** Push a new node after a desired node. */
    public void pushNewNodeAfter(Node _parentNode, Node _newNode){
        if(_parentNode == null){
            System.out.println("The parent node you provided is invalid.");
            return;
        }

        Node parentNodeNext = _parentNode.next;
        _newNode.next = parentNodeNext;
        _parentNode.next = _newNode;
    }

    /** Add to the end. */
    public void addNewNodeToTheEnd(Node _newNode){
        Node temp = head;
        while(temp != null){
            if(temp.next == null){
                temp.next = _newNode;
                break;
            }
            temp = temp.next;
        }
    }

    /** Deleting a Node at a given position. */
    public void deleteNode(int position){
        Node temp = head;
        int index = 0;

        while(temp != null){
            if((index - 1) == position){
                
            }
            temp = temp.next;
            index++;
        }
    }

    /** Driver method, as you call it.  */
    public static void main(String[] args){
        LinkedList linkedList = new LinkedList();

        linkedList.head = new Node("1");
        Node second = new Node("2");
        Node third = new Node("3");
        Node fourth = new Node("4");
        Node fifth = new Node("5");

        linkedList.head.next = second;
        second.next = third;
        third.next = fourth;
        fourth.next = fifth;
        
        linkedList.addNewNode(new Node("6"), fifth);
        linkedList.pushNewNode(new Node("7"));
        linkedList.pushNewNodeAfter(third, new Node("8"));
        linkedList.addNewNodeToTheEnd(new Node("9"));

        linkedList.printList();
    }
}