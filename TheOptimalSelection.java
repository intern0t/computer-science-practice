
/**
 *  Document : TheOptimalSelection.java
 *  GeeksForGeeks Practice (https://practice.geeksforgeeks.org/problems/the-optimal-selection/0)
 *  Submitted by : Prashant Shrestha
 *  Date : 2018-03-17
 */

import java.util.*;
import java.io.*;

class TheOptimalSelection {
    static void Logger(String toPrint, boolean newLine) {
        if (newLine)
            System.out.println(toPrint);
        else
            System.out.print(toPrint);
    }

    static int getTotalPointsFromArray(int[] testData) {
        // Select the # in an array.
        // Get how many are before it ((current index + ) - array.length)
        // selected # * preSelected
        // Sum it all.

        int totalPoints = 0;

        for(int i = 0; i < testData.length; i++){
            totalPoints += (testData[i] * i);
        }

        return totalPoints;
    }

    static String flattenArray(int[] data){
        String toReturn = "";

        for(int i : data){
            toReturn += (i + ", ");
        }

        return toReturn.substring(0, toReturn.length() - 2);
    }

    public static void main(String[] args) {
        // Test data.
        int[] testArrayFirst = { 1, 2, 2, 4, 9 };
        int[] testArraySecond = { 2, 4, 6 };
        int[] testArrayThird = { 2, 2, 2, 2 };

        // Test & Print.
        Logger(flattenArray(testArrayFirst) + " => " + getTotalPointsFromArray(testArrayFirst), true);
        Logger(flattenArray(testArraySecond) + " => " + getTotalPointsFromArray(testArraySecond), true);
        Logger(flattenArray(testArrayThird) + " => " + getTotalPointsFromArray(testArrayThird), true);
    }
}